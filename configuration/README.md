### Configuration

Commento is quite configurable. You're free to inject custom styling CSS, JavaScript, toggle moderation settings, configure SMTP, enable OAuth, and so on. In this section, we document the exhaustive list of settings you can configure in Commento.

{% hint style='info' %}
If you're using the cloud service, you will only be able to configure the frontend (like change the appearance). You will not be able to change backend settings.
{% endhint %}

Configuring the backend and the frontend is done separately. See the corresponding page for more information on how to do so:

 - [**Backend**](backend/README.md): server binding address, setting up PostgreSQL, configuring SMTP, enabling OAuth, configuring a CDN, compression of static assets, and more.

 - [**Frontend**](frontend/README.md): custom CSS styling to change appearance, `<div>` ID, and more.
