### Commento Releases

Commento uses [semantic versioning](https://semver.org) to denote releases. The current major version family is `1.x.y` and the project is considered to be stable and there won't be any breaking changes across releases. It is highly recommended you keep your installation updated for security and performance reasons; Commento will notify you in the logs when a new release is made. Critical security patches are backported to the last two minor releases.

If you download a binary release for your system, please follow the [binary installation instructions](installation/on-your-server/release-binaries.md).

If you are building from source, follow the [source installation instructions](installation/on-your-server/compiling-source.md).

#### Latest Release (v1.4.1)

 - [**Linux** &ndash; `amd64` &ndash; `v1.4.1`](https://commento-release.s3.amazonaws.com/commento-linux-amd64-v1.4.1.tar.gz)  
   `commento-linux-amd64-v1.4.1.tar.gz`  
   <p class="sha">9a013f5d3d2391cef41df8b74fa0a75cabf030a860f8629873814f2cf12742a6</p>

 - [**Source Code** &ndash; `v1.4.1`](https://gitlab.com/commento/commento/-/archive/v1.4.1/commento-v1.4.1.tar.gz)  
   `commento-v1.4.1.tar.gz`  
   <p class="sha">b02e158f85e33fef74641ee899924e701dddb1264119fe3eaf5cbe124aa96393</p>

#### Stable Releases

##### v1.4.1

 - [**Linux** &ndash; `amd64`](https://commento-release.s3.amazonaws.com/commento-linux-amd64-v1.4.1.tar.gz)  
   `commento-linux-amd64-v1.4.1.tar.gz`  
   <p class="sha">9a013f5d3d2391cef41df8b74fa0a75cabf030a860f8629873814f2cf12742a6</p>

 - [**Source Code**](https://gitlab.com/commento/commento/-/archive/v1.4.1/commento-v1.4.1.tar.gz)  
   `commento-v1.4.1.tar.gz`  
   <p class="sha">b02e158f85e33fef74641ee899924e701dddb1264119fe3eaf5cbe124aa96393</p>

##### v1.4.0

 - [**Linux** &ndash; `amd64`](https://commento-release.s3.amazonaws.com/commento-linux-amd64-v1.4.0.tar.gz)  
   `commento-linux-amd64-v1.4.0.tar.gz`  
   <p class="sha">5c24447c7b50c8f35235b57ca5da94a66d15cba1801fb04a900f7ebdff94fcc9</p>

 - [**Source Code**](https://gitlab.com/commento/commento/-/archive/v1.4.0/commento-v1.4.0.tar.gz)  
   `commento-v1.4.0.tar.gz`  
   <p class="sha">3da9165036276c999b4533a27d258b38e80bb6ae1e00f7f13ee202fd836dee86</p>
